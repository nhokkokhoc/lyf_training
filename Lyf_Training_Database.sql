USE [master]
GO
/****** Object:  Database [Student_Manager]    Script Date: 9/25/2019 10:20:38 AM ******/
CREATE DATABASE [Student_Manager]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Student_Manager', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Student_Manager.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Student_Manager_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Student_Manager_log.ldf' , SIZE = 560KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Student_Manager] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Student_Manager].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Student_Manager] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Student_Manager] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Student_Manager] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Student_Manager] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Student_Manager] SET ARITHABORT OFF 
GO
ALTER DATABASE [Student_Manager] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Student_Manager] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Student_Manager] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Student_Manager] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Student_Manager] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Student_Manager] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Student_Manager] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Student_Manager] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Student_Manager] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Student_Manager] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Student_Manager] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Student_Manager] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Student_Manager] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Student_Manager] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Student_Manager] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Student_Manager] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Student_Manager] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Student_Manager] SET RECOVERY FULL 
GO
ALTER DATABASE [Student_Manager] SET  MULTI_USER 
GO
ALTER DATABASE [Student_Manager] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Student_Manager] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Student_Manager] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Student_Manager] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Student_Manager] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Student_Manager', N'ON'
GO
USE [Student_Manager]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 9/25/2019 10:20:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Age] [int] NULL,
	[Address] [nvarchar](100) NULL,
	[Comment] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Student] ON 

INSERT [dbo].[Student] ([ID], [Name], [Password], [Age], [Address], [Comment]) VALUES (1, N'Spiderman', N'1', 19, N'New York', N'shoot web')
INSERT [dbo].[Student] ([ID], [Name], [Password], [Age], [Address], [Comment]) VALUES (2, N'Ironman', N'123', 33, N'New York', N'special suit')
INSERT [dbo].[Student] ([ID], [Name], [Password], [Age], [Address], [Comment]) VALUES (3, N'Thor', N'123456', 35, N'Asgard', N'god of thunder')
SET IDENTITY_INSERT [dbo].[Student] OFF
USE [master]
GO
ALTER DATABASE [Student_Manager] SET  READ_WRITE 
GO
